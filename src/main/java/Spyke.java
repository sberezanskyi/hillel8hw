//Реализуйте взаимоотношения Тома, Джерри и бульдога Спайка с помощью классов и интерфейсов.
// Кот может двигаться и передвигаться, может кого-то съесть или быть съеденным.
// Мышка может передвигаться и быть съеденной.
// Ну а пёс может передвигаться и съесть кого-то (кого захочет!).

public class Spyke {
    public static void main(String[] args) {

    }
    //может двигаться
    public interface Movable {
        void move();
    }

    //может быть съеден
    public interface Eatable {
        void eaten();
    }

    //может кого-нибудь съесть
    public interface Eat {
        void eat();
    }

    public class Cat implements Movable,Eatable,Eat{
        @Override
        public void move() {

        }

        @Override
        public void eat() {

        }

        @Override
        public void eaten() {

        }
    }
    public class Dog implements Movable,Eat{
        @Override
        public void eat() {

        }
        @Override
        public void move() {

        }


    }
    public class Mouse implements Movable,Eatable{
        @Override
        public void move() {

        }

        @Override
        public void eaten() {

        }
    }
}